#include <cassert>
#include <cmath>
#include <exception>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>


/************************************/
// Declarations
/************************************/

//! Our generic exception class, which provides just std::string storage.
class Exception : public std::exception
{
public:

    //! Constructor.
    Exception(const std::string& message);

    //! Overrides what method.
    virtual const char* what() const noexcept override;
    
private:
    
    const std::string message_;
};

//! Class in charge of holding numerator and exponent data together.
template<typename IntT>
class PowerOfTwoApprox
{

public:

    PowerOfTwoApprox(int Nbits, double value);

    //! Compute the best possible approximation of `value` with `Nbits`
    //! \return The approximation as a floating point.    
    double Compute(int Nbits, double value);

    //! Operator for explicit cast to double.
    explicit operator double() const;

    //! Accessor to numerator.
    IntT GetNumerator() const;

    //! Accessor to exponent.
    int GetExponent() const;

private:

    IntT numerator_ {};
    int exponent_ {};
};

/*! 
* \brief Multiply the approximate representation by an integer. 
* 
* \param[in] coefficient Integer coefficient by which the object is multiplied.
* 
* \return An approximate integer result of the multiplication.
*/
template<typename IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx);

//! To enable commutation for operator*
template<typename IntT>
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient);

//! Operator<< for `PowerOfTwoApprox`
template<typename IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation);

//! Abstract class to display
class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay() = 0;

    //! Get the resolution.
    int GetResolution() const;

    //! Pure virtual method Do().
    virtual void operator()(int Nbits) const = 0;

    //! Stupid method to silence the -Wweak-vtables warning - at least one virtual method must be defined outside the class declaration, and virtual method definition unfortunately doesn't count.
    virtual void Unused() const;

protected:

    //! Convenient enum used in \a PrintLine().
    enum class RoundToInteger { no, yes };

    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
   template<typename IntT>
   void PrintLine(int Nbits, double exact, double approx,
                  RoundToInteger do_round_to_integer = RoundToInteger::no,
                  std::string optional_string1 = "", std::string optional_string2 = "") const;

    //! Method to call when an overflow was found.
    //! We didn't go into such precision here, but it might have idea to define a specific 
    //! `Overflow` exception that derives from `Exception` - here we assume all `Exception` 
    //! are overflows.
    void PrintOverflow(int Nbits, const Exception& e) const;                  

private:
    
    //! Resolution.
    const int resolution_; // `const` ensures here that it is defined in the constructor!
  
};



//! Class in charge of the display of a given `PowerOfTwoApprox`.
template<typename IntT>
class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:

    /*!
    * \brief Constructor.
    *
    * Default constructor wouldn't have work, as base class doesn't provide a default constructor
    * and current class needs to construct base class first.
    */
    TestDisplayPowerOfTwoApprox(int resolution);

    //! Destructor. 
    // `virtual` keyword is optional; it doesn't matter much (method would be virtual anyway)
    // `override` keyword is optional but you really should put all the time (if you're using C++ 11 or above).
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Display the output for the chosen `Nbits`.
    virtual void operator()(int Nbits) const override;
        
protected: // as we want inherited class to access it!
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};


//! Class in charge of the display of the sum of 2 `PowerOfTwoApprox` with real coefficients.
template<typename IntT>
class TestDisplaySumOfMultiply : public TestDisplay
{
public:

    //! Constructor.
    TestDisplaySumOfMultiply(int resolution);

    //! Destructor. 
    virtual ~TestDisplaySumOfMultiply() override;
    
    //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits) const override;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const;
    
};


//! Class in charge of displaying information regarding 0.65
template<typename IntT>
class TestDisplayPowerOfTwoApprox065 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:

    //! Convenient alias to the parent.
    //! We could have avoided it by repeating `TestDisplayPowerOfTwoApprox<IntT>` 
    //! but using such an alias (called a 'trait' - we'll see that shortly)
    //! is incredibly more convenient the day you have to refactor and for instance add
    //! another template parameter.
    using parent = TestDisplayPowerOfTwoApprox<IntT>;

    //! Constructor
    TestDisplayPowerOfTwoApprox065(int resolution);

    //! Destructor
    virtual ~TestDisplayPowerOfTwoApprox065() override;

    //! Override of the method that does the work.
    void operator()(int Nbits) const override;

};


//! Class in charge of displaying information regarding 0.35
template<typename IntT>
class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:

    using parent = TestDisplayPowerOfTwoApprox<IntT>;

    TestDisplayPowerOfTwoApprox035(int resolution);

    virtual ~TestDisplayPowerOfTwoApprox035() override;

    void operator()(int Nbits) const override;

};


//! Class intended to store several `TestDisplay` objects.
template<std::size_t CapacityN>
class TestDisplayContainer
{
public:

    //! Constructor
    TestDisplayContainer();

    //! Destructor - as we will need to free properly the memory allocated on destruction.
    ~TestDisplayContainer();

    //! Add a new test_display_register.
    //! At each call, the item to be registered is put at the first available position and internal current_position_
    //! is incremented. If the end-user attempts to register more than three items, an `Exception` is thrown.
    void Register(TestDisplay* test_display);

    //! Return the actual number of elements in the container.
    std::size_t GetSize() const;

    //! Returns the i-th element. If element is invalid, throw an `Exception`.    
    const TestDisplay& operator[](std::size_t i) const;
    
    
private:
    
    //! List of all known `TestDisplay` objects.
    TestDisplay* list_[CapacityN]; // we could have kept the double pointer, but as it is statically known we may also do this!
    
    //! Index to place the next register object. If '3', no more object may be registered.
    std::size_t current_position_ {};
};
    
//! Returns `number` * (2 ^ `exponent`) 
template<typename IntT>
IntT TimesPowerOf2(IntT number, int exponent);

//! Round `x` to the nearest integer.
template<typename IntT>
IntT RoundAsInt(double x);

//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! We'll see much later that it is typically the kind of function that could be put in an anonymous namespace.
template<typename IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator);

//! Maximum integer that might be represented with `nbits` bits.  
template<typename IntT>
IntT MaxInt(int nbits);

//! Function which loops over the number of bits and call the displays.
template<std::size_t CapacityN>
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer<CapacityN>& container);


//! Simply display `integer` on `out` output stream in general case
//! Converts its first to an `int` if it is a `char`.
template<typename IntT>
void DisplayInteger(std::ostream& out, IntT integer);


/************************************/
// Definitions
/************************************/

template<class IntT>
IntT TimesPowerOf2(IntT number, int exponent)
{
    constexpr IntT two = static_cast<IntT>(2);

    while (exponent > 0)
    { 
        IntT product;
        if (__builtin_mul_overflow(number, two, &product))
            throw Exception("Overflow! (in TimesPowerOf2())");

        number = product;
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= two;
        exponent += 1 ; 
    }
    
    return number;
}
    

template<class IntT>
IntT RoundAsInt(double x)
{
    constexpr auto min = static_cast<double>(std::numeric_limits<IntT>::lowest());
    constexpr auto max = static_cast<double>(std::numeric_limits<IntT>::max());

    if (x <= min || x >= max)
    {
        std::ostringstream oconv;
        oconv << "Double '" << x << "' can't be rounded as an integer!";
        throw Exception(oconv.str());
    }

    return static_cast<IntT>(std::round(x));
}


Exception::Exception(const std::string& message)
: message_(message)
{ }


const char* Exception::what() const noexcept
{
    return message_.c_str();
}


template<typename IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator)
{
    denominator = TimesPowerOf2(static_cast<IntT>(1), exponent);   
    numerator = RoundAsInt<IntT>(value * static_cast<double>(denominator));
}


template<typename IntT>
TestDisplayPowerOfTwoApprox<IntT>::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }


template<typename IntT>
TestDisplayPowerOfTwoApprox<IntT>::~TestDisplayPowerOfTwoApprox()  = default;


template<typename IntT>
void TestDisplayPowerOfTwoApprox<IntT>::operator()(int Nbits) const
{
    Display(Nbits, 0.65);
    Display(Nbits, 0.35);    
}


template<typename IntT>
void TestDisplayPowerOfTwoApprox<IntT>::Display(int Nbits, double value) const
{
    try
    {
        PowerOfTwoApprox<IntT> approximation(Nbits, value);

        const double double_quotient = static_cast<double>(approximation);
        
        std::ostringstream oconv;
        oconv << " (" << approximation << ")";

        PrintLine<IntT>(Nbits, value, double_quotient, RoundToInteger::no, "", oconv.str());
        // < here you can't use the default argument for `optional_string1` in `PrintLine` declaration,
        // < as there are non default arguments after it.
    }
    catch(const Exception& e)
    {
        PrintOverflow(Nbits, e);
    }
}


template<typename IntT>
IntT MaxInt(int nbits)
{ 
    return (TimesPowerOf2(static_cast<IntT>(1), nbits) - static_cast<IntT>(1));
}


template<typename IntT>
PowerOfTwoApprox<IntT>::PowerOfTwoApprox(int Nbits, double value)
{
    auto max_numerator = MaxInt<IntT>(Nbits);
    
    auto& numerator = numerator_; // alias!
    auto& exponent = exponent_; // alias!

    IntT denominator {};
    
    do
    {
        // I used here the prefix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    // After the while loop we have numerator > max_numerator,  
    // hence we need to update the fraction using the previous exponent with --exponent.
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


template<typename IntT>
PowerOfTwoApprox<IntT>::operator double() const
{
   IntT denominator = TimesPowerOf2(static_cast<IntT>(1), GetExponent());
   return static_cast<double>(GetNumerator()) / static_cast<double>(denominator);
}


template<typename IntT>
IntT PowerOfTwoApprox<IntT>::GetNumerator() const
{
    return numerator_;
}

template<typename IntT>
int PowerOfTwoApprox<IntT>::GetExponent() const
{
    return exponent_;
}

template<typename IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx)
{
    IntT product;

    if (__builtin_mul_overflow(approx.GetNumerator(), coefficient, &product))
        throw Exception("in Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))");

    return TimesPowerOf2(product, -approx.GetExponent());
}

template<typename IntT>
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient)
{
    return coefficient * approx;
}

template<typename IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation)
{
    DisplayInteger<IntT>(out, approximation.GetNumerator());
    out << " / 2^" << approximation.GetExponent();
    return out;
}


template<typename IntT>
TestDisplaySumOfMultiply<IntT>::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }

template<typename IntT>
TestDisplaySumOfMultiply<IntT>::~TestDisplaySumOfMultiply() = default;


template<typename IntT>
void TestDisplaySumOfMultiply<IntT>::operator()(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);    
}


template<typename IntT>
void TestDisplaySumOfMultiply<IntT>::Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const
{
    try 
    {
        double exact = value1 * static_cast<double>(coefficient1) + value2 * static_cast<double>(coefficient2);

        auto approximation1 = PowerOfTwoApprox<IntT>(Nbits, value1); // auto-to-stick syntax for constructor
        auto approximation2 = PowerOfTwoApprox<IntT>(Nbits, value2);

        IntT part1 = approximation1 * coefficient1;
        IntT part2 = coefficient2 * approximation2;

        IntT computed_approx;

        if (__builtin_add_overflow(part1, part2, &computed_approx))
            throw Exception("Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))");

        std::ostringstream oconv;
        oconv << value1 << " * " << coefficient1 
            << " + " << value2 << " * " << coefficient2 << " = ";

        PrintLine<IntT>(Nbits, exact, static_cast<double>(computed_approx), RoundToInteger::yes, oconv.str());
        // < here we use the default value for the 6-th argument `optional_string2`
    }
    catch(const Exception& e)
    {
        PrintOverflow(Nbits, e);
    }
}


TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


template<typename IntT>
void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            RoundToInteger do_round_to_integer,
                            std::string optional_string1, std::string optional_string2) const
{
    IntT error = RoundAsInt<IntT>(GetResolution() * std::fabs(exact - approx) / exact);
    
    std::cout << "[With " << Nbits << " bits]: " << optional_string1;

    // Here we stop using the ternary operator used so far to handle more properly the warning about
    // long to double conversion. We use a switch but if/else would be almost as fine!
    // (the reason for the "almost" are explained in the dedicated notebook in appendix)
    switch(do_round_to_integer)
    {
        case RoundToInteger::yes:
            std::cout << RoundAsInt<IntT>(exact);
            break;
        case RoundToInteger::no:
            std::cout << exact;
            break;
    }

    std::cout << " ~ " << approx
        << optional_string2
        << "  [error = ";
        
    DisplayInteger(std::cout, error);
    
    std::cout << "/" << GetResolution() << "]" 
        << std::endl;    
}


void TestDisplay::PrintOverflow(int Nbits, const Exception& e) const
{
    std::cout << "[With " << Nbits << " bits]: " << e.what() << std::endl;
}


int TestDisplay::GetResolution() const
{
    return resolution_;
}


void TestDisplay::Unused() const
{
    // We could have left this method empty... The error handling is just to tell user that might think this method
    // calls does anything that it doesn't.
    // In production code I would have used `assert(false && "This method is defined to address the Wweak-vtables 
    // warning; it is not intended to be called!");`; we'll see that later in ErrorHandling notebook.
    throw Exception("This method is defined to address the Wweak-vtables warning; it is not intended to be called!");
}


template<typename IntT>
TestDisplayPowerOfTwoApprox065<IntT>::TestDisplayPowerOfTwoApprox065(int resolution)
: parent(resolution) // see how the trait defined in the class may be used! Of course explicit TestDisplayPowerOfTwoApprox<IntT> would have worked too.
{ }

template<typename IntT>
TestDisplayPowerOfTwoApprox065<IntT>::~TestDisplayPowerOfTwoApprox065() = default;

template<typename IntT>
TestDisplayPowerOfTwoApprox035<IntT>::TestDisplayPowerOfTwoApprox035(int resolution)
: parent(resolution)
{ }

template<typename IntT>
TestDisplayPowerOfTwoApprox035<IntT>::~TestDisplayPowerOfTwoApprox035() = default;


template<typename IntT>
void TestDisplayPowerOfTwoApprox065<IntT>::operator()(int Nbits) const
{
    parent::Display(Nbits, 0.65); // another use for the 'parent' trait!
                                  // this->Display(...) would have worked as well
                                  // but I like the trait better as it gives supplementary
                                  // information about where the methods stems from.
}

template<typename IntT>
void TestDisplayPowerOfTwoApprox035<IntT>::operator()(int Nbits) const
{
    parent::Display(Nbits, 0.35);
}


template<std::size_t CapacityN>
TestDisplayContainer<CapacityN>::TestDisplayContainer()
{ 
    // Allocate nullptr by default is better than undefined behaviour.
    for (auto index = 0ul; index < CapacityN; ++index)
        list_[index] = nullptr;
}


template<std::size_t CapacityN>
TestDisplayContainer<CapacityN>::~TestDisplayContainer()
{
    for (auto index = 0ul; index < CapacityN; ++index)
        delete list_[index];
}


template<std::size_t CapacityN>
void TestDisplayContainer<CapacityN>::Register(TestDisplay* test_display)
{    
    assert(current_position_ <= CapacityN); // we now can activate this pattern we know!
    // < Please notice the difference with the Exception thrown below: as the code is written
    // < current_position_ should never be able to be beyond CapacityN, as the only way to increment
    // < current_position_ is there. However a distracted dev in the future may forget that and
    // < modify the value elsewhere, in which case you will be very happy to understand immediately
    // < what goes wrong...

    if (current_position_ == CapacityN)
        throw Exception("There are already three elements stored in the contained; can't take more!");

    list_[current_position_] = test_display;    
    ++current_position_;
}


// Here we could as well have templatized instead the type of `container` with something like:
// template<class ContainerT>
// void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const ContainerT& container)
template<std::size_t CapacityN>
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer<CapacityN>& container)
{
    // By initializing a local variable `size` in the init part of the loop, we enable more optimizations
    // by the compiler than if we just use `i < GetSize()` as second argument of the for loop.
    for (auto i = 0ul, size = container.GetSize(); i < size; ++i)
    {
        const auto& current_element = container[i];

        for (int nbits = initial_Nbit; nbits <= final_Nbit; nbits += increment_Nbit)
            current_element(nbits);

        std::cout << std::endl;
    }
}


template<std::size_t CapacityN>
std::size_t TestDisplayContainer<CapacityN>::GetSize() const
{
    return current_position_;
}


template<std::size_t CapacityN>
const TestDisplay& TestDisplayContainer<CapacityN>::operator[](std::size_t i) const
{
    if (i >= GetSize())
        throw Exception("You requested an element out of bounds!");

    // In real code I would also put sanity check
    // assert(list_[i] != nullptr); 
    // which should be true if first condition is right!

    return *(list_[i]);
}


template<typename IntT>
void DisplayInteger(std::ostream& out, IntT integer)
{
    out << integer;
}


template<>
void DisplayInteger(std::ostream& out, char integer)
{
    out << static_cast<int>(integer);
}



/************************************/
// Main function
/************************************/

// [[maybe_unused]] is a C++ 17 keyword to indicate we're fully aware the variable may not be used.
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    try
    {     

        TestDisplayContainer<2ul> container;

        using type = char;

        container.Register(new TestDisplayPowerOfTwoApprox065<type>(1000));
        container.Register(new TestDisplayPowerOfTwoApprox035<type>(1000));
        //container.Register(new TestDisplaySumOfMultiply<type>(1000000));
        
        Loop(1, 4, 1, container);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

