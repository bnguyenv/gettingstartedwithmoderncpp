#pragma once

#include "Exception.hpp"
#include "PowerOfTwoApprox.hpp"


template<typename IntT>
TestDisplayPowerOfTwoApprox<IntT>::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }


template<typename IntT>
TestDisplayPowerOfTwoApprox<IntT>::~TestDisplayPowerOfTwoApprox()  = default;


template<typename IntT>
void TestDisplayPowerOfTwoApprox<IntT>::operator()(int Nbits) const
{
    Display(Nbits, 0.65);
    Display(Nbits, 0.35);    
}


template<typename IntT>
void TestDisplayPowerOfTwoApprox<IntT>::Display(int Nbits, double value) const
{
    try
    {
        PowerOfTwoApprox<IntT> approximation(Nbits, value);

        const double double_quotient = static_cast<double>(approximation);
        
        std::ostringstream oconv;
        oconv << " (" << approximation << ")";

        PrintLine<IntT>(Nbits, value, double_quotient, RoundToInteger::no, "", oconv.str());
        // < here you can't use the default argument for `optional_string1` in `PrintLine` declaration,
        // < as there are non default arguments after it.
    }
    catch(const Exception& e)
    {
        PrintOverflow(Nbits, e);
    }
}


template<typename IntT>
TestDisplayPowerOfTwoApprox065<IntT>::TestDisplayPowerOfTwoApprox065(int resolution)
: parent(resolution) // see how the trait defined in the class may be used! Of course explicit TestDisplayPowerOfTwoApprox<IntT> would have worked too.
{ }

template<typename IntT>
TestDisplayPowerOfTwoApprox065<IntT>::~TestDisplayPowerOfTwoApprox065() = default;

template<typename IntT>
TestDisplayPowerOfTwoApprox035<IntT>::TestDisplayPowerOfTwoApprox035(int resolution)
: parent(resolution)
{ }

template<typename IntT>
TestDisplayPowerOfTwoApprox035<IntT>::~TestDisplayPowerOfTwoApprox035() = default;


template<typename IntT>
void TestDisplayPowerOfTwoApprox065<IntT>::operator()(int Nbits) const
{
    parent::Display(Nbits, 0.65); // another use for the 'parent' trait!
                                  // this->Display(...) would have worked as well
                                  // but I like the trait better as it gives supplementary
                                  // information about where the methods stems from.
}

template<typename IntT>
void TestDisplayPowerOfTwoApprox035<IntT>::operator()(int Nbits) const
{
    parent::Display(Nbits, 0.35);
}
