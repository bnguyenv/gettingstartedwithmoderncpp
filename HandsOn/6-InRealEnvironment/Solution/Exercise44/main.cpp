#include <memory>
#include <vector>

#include "TestDisplayPowerOfTwoApprox.hpp"
#include "TestDisplaySumOfMultiply.hpp"


/************************************/
// Declarations
/************************************/

using TestDisplayContainer = std::vector<std::unique_ptr<TestDisplay>>;

//! Function which loops over the number of bits and call the displays.
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer& container);


/************************************/
// Definitions
/************************************/

void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer& container)
{
    for (const auto& current_element_ptr : container)
    {
        const auto& current_element = *current_element_ptr;

        for (int nbits = initial_Nbit; nbits <= final_Nbit; nbits += increment_Nbit)
            current_element(nbits);

        std::cout << std::endl;
    }
}

/************************************/
// Main function
/************************************/

// [[maybe_unused]] is a C++ 17 keyword to indicate we're fully aware the variable may not be used.
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    try
    {     
        TestDisplayContainer container;

        using type = long;

        container.emplace_back(std::make_unique<TestDisplayPowerOfTwoApprox065<type>>(100000000));
        container.emplace_back(std::make_unique<TestDisplayPowerOfTwoApprox035<type>>(100000000));
        container.emplace_back(std::make_unique<TestDisplaySumOfMultiply<type>>(1000000));
        
        Loop(4, 32, 4, container);
    }
    catch(const std::exception& e) // we could trap only `Exception`, but it's not a bad idea to also
                                   // catch exceptions from STL...
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

