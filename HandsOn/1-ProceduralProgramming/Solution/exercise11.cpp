#include <cmath>
#include <fstream>
#include <iostream>
#include <string>


/************************************/
// Declarations
/************************************/

//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent);

//! Round `x` to the nearest integer.
int RoundAsInt(double x);

//! Function for error handling. We will see later how to fulfill the same functionality more properly.
//! Don't bother here about [[noreturn]] - it's just a keyword to silence a possible warning telling
//! the program may not return at the calling site (which is definitely the case here as there is a 
//! std::exit() called in the function).
[[noreturn]] void Error(std::string explanation);

//! Display the approximation of the given argument that does not exceed the chosen maximum numerator.
void DisplayPowerOf2Approx(std::ostream& out, int max_numerator, double value);

//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! We'll see much later that it is typically the kind of function that could be put in an anonymous namespace.
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator);

//! Maximum integer that might be represented with `nbits` bits.  
int MaxInt(int nbits);

//! Compute the best possible approximation of `value` with `Nbits`
//! \param[out] numerator Computed numerator.
//! \param[out] exponent Computed exponent.
//! \return The approximation as a floating point.
double ComputePowerOf2Approx(int Nbits, double value, int& numerator, int& exponent);

//! Multiply an approximated value by an integer.
int Multiply(int Nbits, double value, int coefficient);

//! Compute value1 * coefficient1 + value2 * coefficient2 over Nbits bits.
void DisplaySumOfMultiply(std::ostream& out, int Nbits, double value1, int coefficient1, double value2, int coefficient2);


/************************************/
// Definitions
/************************************/

int TimesPowerOf2(int number, int exponent)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

int RoundAsInt(double x)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    return static_cast<int>(std::round(x));
}


[[noreturn]] void Error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}


void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}


void DisplayPowerOf2Approx(std::ostream& out, int Nbits, double value)
{
    int numerator {}, exponent {};
    double double_quotient = ComputePowerOf2Approx(Nbits, value, numerator, exponent);
        
    const double error = std::fabs(value - double_quotient) / value;
        
    out << "[With " << Nbits << " bits]: " << value << " ~ " << double_quotient << " (" << numerator << 
        " / 2^" << exponent << ")  [error = " << RoundAsInt(100. * error) << "/100]" << std::endl;
}


int MaxInt(int nbits)
{ 
    return (TimesPowerOf2(1, nbits) - 1);
}


double ComputePowerOf2Approx(int Nbits, double value, int& numerator, int& exponent)
{
    int max_numerator = MaxInt(Nbits);
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the prefix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    // After the while loop we have numerator > max_numerator,  
    // hence we need to update the fraction using the previous exponent with --exponent.
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
    
    return static_cast<double>(numerator) / denominator;     
}


int Multiply(int Nbits, double value, int coefficient)
{
    int numerator {}, exponent {};
    ComputePowerOf2Approx(Nbits, value, numerator, exponent);

    return TimesPowerOf2(numerator * coefficient, -exponent);
}


void DisplaySumOfMultiply(std::ostream& out, int Nbits, double value1, int coefficient1, double value2, int coefficient2)
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    int rounded = RoundAsInt(exact);
    int computed_approx = Multiply(Nbits, value1, coefficient1) + Multiply(Nbits, value2, coefficient2);

    const double error = std::fabs(exact - computed_approx) / exact;

    out << "[With " << Nbits << " bits]: " << value1 << " * " << coefficient1 
        << " + " << value2 << " * " << coefficient2 << " = " 
        << rounded << " ~ " << computed_approx 
        << "  [error = " << RoundAsInt(1000. * error) <<  "/1000]" <<  std::endl;
}



/************************************/
// Main function
/************************************/

// [[maybe_unused]] is a C++ 17 keyword to indicate we're fully aware the variable may not be used.
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{        
    std::ofstream out("/tmp/approx_0.65.txt");

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(out, nbits, 0.65);

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(std::cout, nbits, 0.35);
    
    std::cout << std::endl;
    
    for (int nbits = 1; nbits <= 8; ++nbits)
        DisplaySumOfMultiply(std::cout, nbits, 0.65, 3515, 0.35, 4832); // to compute 0.65 * 3515 + 0.35 * 4832
    
    return EXIT_SUCCESS;
}

