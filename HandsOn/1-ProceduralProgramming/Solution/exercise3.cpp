#include <cmath>
#include <iostream>
#include <string>


/************************************/
// Declarations
/************************************/

//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent);

//! Round `x` to the nearest integer.
int RoundAsInt(double x);

//! Function for error handling. We will see later how to fulfill the same functionality more properly.
//! Don't bother here about [[noreturn]] - it's just a keyword to silence a possible warning telling
//! the program may not return at the calling site (which is definitely the case here as there is a 
//! std::exit() called in the function).
[[noreturn]] void Error(std::string explanation);

//! Display the approximation of the given argument for the exponents from 1 to 8.
void DisplayPowerOf2Approx(double value);



/************************************/
// Definitions
/************************************/

int TimesPowerOf2(int number, int exponent)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

int RoundAsInt(double x)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    return static_cast<int>(std::round(x));
}


[[noreturn]] void Error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}


void DisplayPowerOf2Approx(double value)
{
    for (int exponent = 1; exponent <= 8; ++exponent)
    {
        int denominator = TimesPowerOf2(1, exponent);        
        int numerator = RoundAsInt(value * denominator);
        double double_quotient = static_cast<double>(numerator) / denominator; 
        
        std::cout << value << " ~ " << double_quotient << " (" << numerator << 
            " / 2^" << exponent << ')' << std::endl;
    }
    
    std::cout << std::endl;
}


/************************************/
// Main function
/************************************/

// [[maybe_unused]] is a C++ 17 keyword to indicate we're fully aware the variable may not be used.
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    DisplayPowerOf2Approx(.65);
    DisplayPowerOf2Approx(.35);
    
    return EXIT_SUCCESS;
}

