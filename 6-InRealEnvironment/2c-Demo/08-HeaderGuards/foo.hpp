#ifndef FOO_HPP // If this macro is not yet defined, proceed to the rest of the file.
#define FOO_HPP // Immediately define it so next call won't include again the file content.

class Foo
{ };

#endif // FOO_HPP // End of the macro block that begun with #ifndef