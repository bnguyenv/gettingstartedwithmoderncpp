// File hello.cpp
#include <iostream>
#include "hello.hpp"
#include "who-are-you.hpp"

void hello()
{
    auto identity = WhoAreYou();
    std::cout << "Hello " << identity << '!' << std::endl;
}